#!/bin/sh

for f in Data/*/
do
	echo "$f"
	cd $f
	sbatch -t 00:10:00 --wrap="python3 ../../Code/CalculateDivergence.py -out divergence.csv"
	cd ../../
done
