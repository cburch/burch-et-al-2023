# README #

This repository contains the Python code and shell scripts that comprise the analysis pipeline in:

Burch, C. L., Romanchuk, A., Kelly, M., Wu, Y., and Jones, C. D.  2023. Empirical evidence that complexity limits horizontal gene transfer. Genome Biology & Evolution. Vol TBD: Pages TBD.

## Required Software and Versions Used: ##
* python/3.9.6
* bwa/0.7.17
* samtools/1.14
* bedtools/2.29
* gcc/6.3.0   
* blast/2.12.0
* r/3.3.1


## Code Files: ##
* probcons_v1_12_repaired.tar.gz | Probcons multiple alignment software (http://probcons.stanford.edu/), debugged to run on our unix server
* convert_gb_to_fasta_protein.py | Takes a genbank file as input and outputs a fasta file containing the protein sequence of all CDS features in the input file
* CombineReciprocalBlastResults.py | Reads in the forward and reverse blast results and compares them to identify reciprocal best hits. Writes reciprocal best hits to file.
* MakeAlignments.py | calls Probcons to align E. coli and query genome sequences for all genes in the query genome that were identified as a reciprocal best hit to an E. coli gene.
* CalculateDivergence.py | Takes pairwise protein sequence alignments as input and outputs a CSV file with the % amino acid difference calculated from each pair of aligned sequences.
* Combine_Trace_and_Info.py | Takes as input the whole genome shotgun trace and ancillary information files from NCBI. Outputs a fasta file, WGS.fasta.trace, that replaces the read names in the trace file with important read characteristics from the information file.

## Shell Scripts: ##
* genbank_to_protein_script.sh | moves into every directory inside Data/ and runs the program convert_gb_to_fasta_protein.py on every .gb file in the directory 
* Make_Blast_Databases.sh | moves into every directory inside Data/, concatenates the .faa files into a single concatenated.faa file, and makes a Blast database from concatenated.faa
* run_forward_BLAST.sh | moves into every directory inside Data/ and blasts the gene sequences in concatenated.faa (in that directory) against the E. coli Blast database 
* run_reverse_BLAST.sh | moves into every directory inside Data/ and blasts the E. coli gene sequences against the concatenated.faa Blast database (in that directory)
* run_CombineReciprocalBlastResults.sh | moves into every directory inside Data/ and runs CombineReciprocalBlastResults.py
* run_Alignments.sh | moves into every directory inside Data/ and runs MakeAlignments.py
* run_Divergence.sh | moves into every directory inside Data/ and runs CalculateDivergence.py
* run_trace_and_info.sh | moves into every directory inside Data/ and runs Combine_Trace_and_Info.py
* index_script.sh | uses BWA to index the concatenated fasta files in each Data/Genome/ directory
* align_script.sh | uses BWA to map the whole genome shotgun reads from WGS.fasta.trace in each Data/Genome/ directory to their assembled genome. 
* bedfiles_script.sh | takes as input the mapped reads and outputs a count of the number of reads that completely span each gene in the genome.

## Running the code ##

### Organize files into Data and Code directories: ###

- Data/
	- Genome_1/
		- Genome_1.fasta
		- Genome_1.gb
	- Genome_2/
		- Genome_2.fasta
		- Genome_2.gb
- Code/


### Step 1: Divergence Pipeline: ###

Run in sequence:

1. genbank_to_protein_script.sh
2. Make_Blast_Databases.sh
3. run_forward_BLAST.sh
4. run_reverse_BLAST.sh
5. run_CombineReciprocalBlastResults.sh
6. run_Alignments.sh
7. run_Divergence.sh


### Step 2: Coverage Pipeline ###

Run in sequence:

1. run_trace_and_info.sh
2. index_script.sh
3. align_script.sh
4. bedfiles_script.sh


## Code authors: ##
* Christina Burch, cburch@bio.unc.edu
* Michael Kelly, mkelly95@live.unc.edu 
* Yingfang Wu, yfwu1123@gmail.com
* Artur Romanchuk, artur.romanchuk@gmail.com
* Corbin Jones, cdjones@bio.unc.edu