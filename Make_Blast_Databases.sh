#!/bin/sh

for f in Data/*/
do
	echo "$f"
	cd $f
	cat *.faa > concatenated.faa
	sbatch -t 00:10:00 --wrap="makeblastdb -in concatenated.faa -dbtype prot -parse_seqids"
	cd ../../
done
cd ..
