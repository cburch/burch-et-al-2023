from Bio import SeqIO
import glob
import argparse


# In[ ]:

#read file names from command line
#Blast query genome against E. coli = forward
#Blast E. coli against query genome = reverse
parser = argparse.ArgumentParser()
parser.add_argument('-forward', type=str, help='Blast results filename for Query against E. coli')
parser.add_argument('-reverse', type=str, help='Blast results filename for E. coli against Query')
args = parser.parse_args()
print(args)


# In[4]:

#open blast results
coli_blast_results_file = open(args.reverse, 'r')

coli_lines = []
for line in coli_blast_results_file:#
   coli_lines.append(line)
print(coli_lines[10])


# In[12]:

output_file = open("Reciprocal_"+args.forward, 'w')
#open blast results
query_blast_results_file = open(args.forward, 'r')

for line in query_blast_results_file:
    #get query seq id from blast results
    id1 = line.split('\t')[0]
    #get coli seq id from blast results
    id2 = line.split('\t')[1]
    #find reciprocal result for that coli gine
    match = [s for s in coli_lines if id2 in s]
    if (match):
        #print "\n" + str(match[0].split('\t'))
        #print line
        if (match[0].split('\t')[1] in id1):
            print("Reciprocal match!")
            output_file.write(line)
        else:
            print("\n" + str(match[0].split('\t')))
            print(line)
            print("No Reciprocal match")
output_file.close()
