#!/bin/sh

for f in Data/*/
do
	echo "$f"
	cd $f
	rm -r ALIGNs
	mkdir ALIGNs
	sbatch -t 00:20:00 --wrap="python3 ../../Code/MakeAlignments.py -ref ../escherichia_coli_K12_substr_MG1655/concatenated.faa -query concatenated.faa -blast Reciprocal_BLAST_results.txt"
	cd ../../
done
