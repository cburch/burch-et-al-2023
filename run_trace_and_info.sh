#!/bin/sh

for d in Data/*/;
do
	echo "$d"
	cd $d
	sbatch -t 00:60:00 --wrap="python3 ../../Code/Combine_Trace_and_Info.py"
	cd ../../
done
