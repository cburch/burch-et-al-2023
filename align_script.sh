#!/bin/sh

for d in Data/*/;
do
	cd $d
	pwd
	sbatch -t 00:20:00 --wrap="bwa bwasw -t 3 -f aligned_reads.sam concatenated.fasta WGS.fasta.trace"
	cd ../../
done
