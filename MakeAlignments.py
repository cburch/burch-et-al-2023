import os
from Bio import SeqIO
from subprocess import call
import glob
import argparse

#read file names from command line
parser = argparse.ArgumentParser()
parser.add_argument('-ref', type=str, help='name of E coli protein fasta reference file')
parser.add_argument('-query', type=str, help='name of Query genome protein fasta file')
parser.add_argument('-blast', type=str, help='name of BLAST results file')
args = parser.parse_args()
print(args)

#get E. coli gene protein sequences
coli_genes = []
for record in SeqIO.parse(open(args.ref, "r"), "fasta") :
    coli_genes.append(record)
print(len(coli_genes))

#get query gene protein sequences
query_genes = []
for record in SeqIO.parse(open(args.query, "r"), "fasta") :
    query_genes.append(record)
print(len(query_genes))

#open blast results
blast_results_file = open(args.blast, 'r')

i=0
for line in blast_results_file:
    #get query seq id from blast results
    id1 = line.split('\t')[0]
    #find query gene in query genome
    gene1 = next((gene for gene in query_genes if id1 in gene.id), None)
    seq1 = str(gene1.seq)
    #get coli seq id from blast results
    id2 = line.split('\t')[1]
    #find coli gene in coli genome
    gene2 = next((gene for gene in coli_genes if id2 in gene.id), None)
    seq2 = str(gene2.seq)
    #get length of sequence match from blast results
    match_length = float(line.split('\t')[3])
    #require that the sequence match is most of the length of the gene
    #write good matches to individual ALIGN#.fasta files
    ratio = match_length/float(min(len(seq1),len(seq2)))
    if ratio > 0.3:
        i+=1
        #Write the sequences to file
        output_handle = open('ALIGNs/ALIGN#'+str(i)+'.fasta', 'w')
        SeqIO.write([gene1, gene2], output_handle, "fasta")
        output_handle.close()
        #Align the sequences using PROBCONS
        input_file = 'ALIGNs/ALIGN#'+str(i)+'.fasta'
        output_file = open('ALIGNs/ALIGN#'+str(i)+'.PC.fasta', "w")
        call(["../../Code/probcons/probcons",input_file],stdout=output_file)
        output_file.close()
        
#number good matches
print(i)



