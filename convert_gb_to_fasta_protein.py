# -*- coding: utf-8 -*-

#IMPORT
from Bio import SeqIO
import argparse

#read file names from command line
parser = argparse.ArgumentParser()
parser.add_argument('-genbankinput', type=str, help='name of GenBank file that will be used to find CDS sequences')
parser.add_argument('-outputfile', type=str, help='name of protein fasta file to be written as output')
args = parser.parse_args()
print(args)

#generate fasta protein files from genbank genome files
i=0
file = open(args.outputfile, 'a')
for record in SeqIO.parse(open(args.genbankinput, "r"), "genbank"):
    for feature in record.features:
        if feature.type=="CDS":
            try:
                feature.qualifiers['translation']
            except NameError:
                var_exists = False
            except KeyError:
                var_exists = False
            else:
                var_exists = True
            if var_exists:
                try:
                    feature.location
                except NameError:
               	    var2_exists = False
                except KeyError:
                    var2_exists = False
                else:
                    var2_exists = True 
                if var2_exists:
                    i += 1
                    loc = str(feature.location)[1:]
                    strand = loc[-2]
                    start = loc.split(":")[0]
                    end = loc.split(":")[1].split("]")[0]
                    prot = feature.qualifiers['protein_id'][0]
                    if strand=="+":
                        location=start+".."+end
                    elif strand=="-":
                        location="complement("+start+".."+end+")"
                    else:
                        location=""
                    file.write(">lcl|"+record.id+"_prot_"+prot+"_"+str(i)+" [locus_tag="+feature.qualifiers['locus_tag'][0]+"] [protein="+feature.qualifiers['product'][0]+'] [location='+location+']\n')
                    file.write(feature.qualifiers['translation'][0]+'\n')
file.close()
