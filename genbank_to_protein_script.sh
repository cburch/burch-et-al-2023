#!/bin/sh

for d in Data/*/;
do
	cd $d
	echo "$d"
	rm *.faa
	for g in *.gb;
	do
		fname=${g//.gb/}
		sbatch -t 00:10:00 --wrap="python3 /pine/scr/c/b/cburch/WGS/Code/convert_gb_to_fasta_protein.py -genbankinput $g -outputfile $fname.faa"
	done
	cd /pine/scr/c/b/cburch/WGS/
done
