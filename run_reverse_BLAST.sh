#!/bin/sh

for f in Data/*/
do
	echo "$f"
	cd $f
	sbatch -t 00:10:00 --wrap="blastp -db concatenated.faa -query /pine/scr/c/b/cburch/WGS/Data/escherichia_coli_K12_substr_MG1655/concatenated.faa -outfmt 6 -evalue 1e-6 -max_target_seqs 1 -out reverse_BLAST_results.txt"
	cd ../../
done
