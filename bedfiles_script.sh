#!/bin/sh

# This script is meant to be run inside an interactive shell

for d in Data/*/;
do
	cd $d
	echo "$d"
	samtools view -bShu aligned_reads.sam > aligned_reads.bam
	samtools sort aligned_reads.bam -o aligned_sorted.bam
	bedtools bamtobed -i aligned_sorted.bam > aligned_sorted.bed
	python3 /pine/scr/c/b/cburch/WGS/Code/pair_reads_to_corresponding_templateID.py
	python3 /pine/scr/c/b/cburch/WGS/Code/FastaProtein2BED_locus_tags.py -fastainput concatenated.faa -outputfile gene_locations.bed
	bedtools intersect -a gene_locations.bed -b paired_WGS.bed -c -f 1 > intersect.bed
	cd /pine/scr/c/b/cburch/WGS/
done
