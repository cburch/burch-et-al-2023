#!/usr/bin/env python
# coding: utf-8

from Bio import SeqIO
import fnmatch
import os


# find trace and anciliary info files  
for file in os.listdir('.'):
    if fnmatch.fnmatch(file, 'anc.*.001'):
        anc_file = file
    if fnmatch.fnmatch(file, 'fasta.*.001'):
        fasta_file = file
print(anc_file)
print(fasta_file)


#open data files 
infofile = open(anc_file,"r")
tracefile = open(fasta_file,"r")


#specify output files
WGSfastafile = open("WGS.fasta.trace","w")
logfile = open("log.txt","w")


#read first line to get header info
infoline =infofile.readline()
infoparameters = infoline.split('\t')
if ("TRACE_TYPE_CODE" in infoline):
    trace_type_column = infoparameters.index("TRACE_TYPE_CODE")
    template_id_column = infoparameters.index("TEMPLATE_ID")
    read_id_column = infoparameters.index("TI")
    read_direction_column = infoparameters.index("TRACE_END")


#check that all columns were identified 
print(trace_type_column)
print(template_id_column)
print(read_id_column)
print(read_direction_column)


for record in SeqIO.parse(tracefile,"fasta"):
    read_id = record.id.split('|')[2]
    print(read_id)
    infoline =infofile.readline()
    infoparameters = infoline.split('\t')
    if read_id == infoparameters[read_id_column]:
        if infoparameters[trace_type_column] == 'WGS':
            output1 = ">gn1|ti|"+infoparameters[template_id_column]+"|"+infoparameters[read_direction_column]+"|"+read_id+"\n"
            WGSfastafile.write(output1)
            output2 = str(record.seq)+"\n"
            WGSfastafile.write(output2)
    else:
        log = "Read IDs don't match: FASTA-"+read_id+" ANC-"+infoparameters[read_id_column]+"\n"
        logfile.write(log)
WGSfastafile.close()
logfile.close()
tracefile.close()
infofile.close()
