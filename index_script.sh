#!/bin/sh

for d in Data/*/;
do
	echo "$d"
	cd $d
	cat *.fasta > concatenated.fasta
	sbatch -t 00:20:00 --wrap="bwa index concatenated.fasta"
	cd ../../
done
