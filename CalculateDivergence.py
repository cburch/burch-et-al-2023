#IMPORT
import os, sys
from subprocess import call
from Bio import SeqIO
from Bio import AlignIO
from Bio.Seq import Seq
import numpy as np
import string
import glob
import argparse

#read file names from command line
parser = argparse.ArgumentParser()
parser.add_argument('-out', type=str, help='name of output file')
args = parser.parse_args()
print(args)

out_file = open(args.out, 'w')
#write header
out_file.write('Gene_ID,Query_Locus_Tag,Query_Gene_Name,Ecoli_Locus_Tag,Ecoli_Gene_Name,Percent_Identity\n')
    
#find aligned protein sequences
files = glob.glob('ALIGNs/*.PC.fasta')
for file in files:
    alignment = AlignIO.read(file, 'fasta')

    i=0 # counts identity hits with ecoli (2nd sequence)
    j=0 # counts positions in first sequence
    for amino_acid in alignment[0].seq:
        if amino_acid == '-':
            pass
        else:
            if amino_acid == alignment[1].seq[j]:
                i += 1
        j += 1
    seq = str(alignment[0].seq)
    gap_strip = seq.replace('-', '')
    percent = 100*i/len(gap_strip)

    query_details = alignment[0].description.split(' ')
    query_locus_tag = next((item for item in query_details if 'locus_tag' in item), None)
    query_locus_tag = query_locus_tag.split('=')[1][:-1]
    query_gene_name = next((item for item in query_details if 'gene=' in item), 'None')
    if query_gene_name != 'None':
        query_gene_name = query_gene_name.split('=')[1][:-1]

    coli_details = alignment[1].description.split(' ')
    coli_locus_tag = next((item for item in coli_details if 'locus_tag' in item), None)
    coli_locus_tag = coli_locus_tag.split('=')[1][:-1]
    coli_gene_name = next((item for item in coli_details if 'gene=' in item), 'None')
    if coli_gene_name != 'None':
        coli_gene_name = coli_gene_name.split('=')[1][:-1]
        
    out_file.write(query_details[0]+','+query_locus_tag+','+query_gene_name+','+coli_locus_tag+','+coli_gene_name+','+str(percent)+'\n')
out_file.close()
