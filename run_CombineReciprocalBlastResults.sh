#!/bin/sh

for f in Data/*/
do
	echo "$f"
	cd $f
	sbatch -t 00:10:00 --wrap="python3 ../../Code/CombineReciprocalBlastResults.py -forward BLAST_results.txt -reverse reverse_BLAST_results.txt"
	cd ../../
done
